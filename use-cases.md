Opis skrócony przypadków użycia
===============================

Aktorzy procesu i ich cele
--------------------------

Aktor       Cel 
----------- -----------------------------
Klient   -   Wypłata gotówki.
	   deponowanie gotówki,
               sprawdzenie stanu konta,
               zakup kodu do telefonii pre-paidowej,
               wykonanie przelewu.
               
Serwisant -  Uzupełnienie gotówki w bankomacie,
	      podmienienie kasetki z wpłaconymi banknotami na pustą.
                  

Słownik
-----------

Hasło       Opis
----------- -----------------------------
Konto   -   Rachunek w księgowości bankowej wykazujący stan pieniędzy klienta

Wypłata  -    Podjęcie określonej sumy pieniężnej z konta.

Deponowanie    -  Dodanie środków do konta.

Stan konta  -    Ilość dostępnych środków na koncie.

Telefonia Pre-paid   -   Usługa telefoniczna "na kartę" nie będąca abonamentem.

Przelew   -   Przekazanie środków

Użytkownik/Klient    -  Osoba korzystająca z systemu.

Bankomat    -  Urządzenie korzystające z systemu.

PIN    -  Numer zabezpieczające kartę.

Karta   -   Identyfikator klienta.



Przypadki użycia
----------------

### Use case 1: Autoryzacja Klienta

Klient wkłada kartę do bankomatu. Bankomat prosi o wprowadzenie PINu zabezpieczającego kartę. Klient podaje PIN do karty. Bankomat potwierdza prawidłowość PINu i wyświetla możliwe opcje wyboru.


### Use case 2: Wypłata gotówki

Klient po autoryzacji wybiera opcję wypłaty gotówki. Bankomat wyświetla proponowane kwoty wypłaty lub możliwość wprowadzenia własnej kwoty. Klient wybiera pożądaną kwotę. Bankomat pyta czy wydrukować potwierdzenie. Klient deklaruje chęć otrzymania potwierdzenia. Bankomat stwierdza prawidłowość kwoty, wydaje kartę. Klient zabiera kartę. Bankomat wydaje gotówkę i potwierdzenie. Klient zabiera gotówkę i potwierdzenie.

### Use case 3: Deponowanie gotówki

Klient po autoryzacji wybiera opcję deponowania gotówki. Bankomat prosi o wprowadzenie kwoty wpłaty. Klient podaje kwotę wpłaty. Bankomat pyta o ilość banknotów do wpłacania. Klient podaje ilość banknotów, które chce wpłacić. Bankomat stwierdza wystarczającą ilość miejsca w kasetce na wpłaty i wyświetla komunikat o konieczności wprowadzenia zadeklarowanej kwoty w wyznaczone miejsce. Klient wprowadza gotówkę. Bankomat sprawdza wprowadzoną kwotę i wyświetla listę wprowadzonych banknotów prosząc klienta o potwierdzenie zgodności. Klient potwierdza poprawność.Bankomat pyta czy wydrukować potwierdzenie. Klient deklaruje chęć otrzymania potwierdzenia. Bankomat wydaje w kolejności kartę i potwierdzenie. Klient zabiera kartę i potwierdzenie.

### Use case 4: Sprawdzenie stanu konta

Klient po autoryzacji wybiera opcję sprawdzenie stanu konta. Bankomat wyświetla stan konta i pytanie czy wydrukować informacje o stanie konta. Klient zapoznaje się ze stanem konta i deklaruje chęć wydruku informacji. Bankomat wydaje w kolejności kartę i potwierdzenie. Klient zabiera kartę i potwierdzenie.

### Use case 5: Zakup kodu do telefonii pre-paidowej 

Klient po autoryzacji wybiera opcję doładowania telefonii Pre-Paid. Bankomat wyświetla zapytanie o wybór operatora a następnie o kwotę. Klient wprowadza dane. Bankomat potwierdza poprawność danych i wykonuje transakcję. Bankomat wydaje kartę i potwierdzenie z kodem do doładowania telefonu.

### Use case 6: Wykonanie przelewu

Klient po autoryzacji wybiera opcję przelewu. Bankomat zweryfikował, że klient ma nadane w banku uprawnienia do wykonywania przelewów za pomocą bankomatu. Bankomat wyświetla w zadanej kolejności pytania: o numer konta odbiorcy, kwotę przelewu, tytuł przelewu. Klient wprowadza i zatwierdza dane. Bankomat wyświetla przyjęte dane do przelewu i pyta o ich poprawność. Klient potwierdza poprawność wprowadzonych danych. Bankomat potwierdza w systemie poprawny numer konta i wystarczającą ilość środków na wykonanie operacji i wykonuje transakcję. Bankomat wydaje kartę i potwierdzenie.



### Use case 7: Uzupełnienie gotówki w bankomacie i podmiana kasetki z wpłaconymi banknotami na pustą

Serwisant w asyście 2 uzbrojonych ochroniarzy otwiera bankomat zdejmując zabezpieczenia fizyczne bankomatu i dostaje się do kasetek z pieniędzmi. Serwisant uzupełnia brakujące banknoty w kasetkach ilością wskazaną odgórnie przez bank. Serwisant podmienia kasetkę z wpłaconymi banknotami na pustą. Serwisant zamyka bankomat i wkłada kartę serwisową do bankomatu. Bankomat prosi o PIN zabezpieczający kartę serwisową. Serwisant wpisuje PIN do karty. Bankomat potwierdza prawidłowość PINu i w następującej kolejności zadaje pytania o liczbę dodanych banknotów w kasetkach o nominale: 200 zł, następnie 100 zł, 50 zł, 20 zł, 10 zł. Serwisant odpowiednio wpisuje poprawną liczbę uzupełnionych banknotów i zatwierdzając każdą z osobna. Bankomat wyświetla pytanie czy kasetka na wpłacone banknoty została opróżniona. Serwisant potwierdza opróżnienie kasetki. Bankomat wyświetla wszystkie wprowadzone dane przez serwisanta i prosi o potwierdzenie poprawności. Serwisant potwierdza poprawność. Bankomat  aktualizuje informacje z wewnętrzną bazą i oddaje kartę.
