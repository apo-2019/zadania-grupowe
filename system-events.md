Zdarzenia systemowe
===================

Use case 1: Autoryzacja Klienta
-----------------

  - Włożenie karty.
  - Wprowadzenie PIN'u.

Use case 2: Wypłata gotówki
-----------------

  - Wybranie opcji wypłaty gotówki.
  - Wprowadzenie kwoty wypłaty.
  - Zadeklarowanie chęci wydrukowania potwierdzenia.
  - Zabranie karty.
  - Zabranie banknotów.
  - Zabranie potwierdzenia.

Use case 3: Deponowanie gotówki
-----------------

  - Wybranie opcji deponowanie gotówki.
  - Wprowadzenie kwoty wpłaty.
  - Wprowadzenie liczby banknotów do wpłacenia.
  - Włożenie banknotów.
  - Potwierdzenie poprawności wprowadzonej kwoty (?)
  - Zadeklarowanie chęci wydrukowania potwierdzenia.
  - Zabranie karty.
  - Zabranie potwierdzenia.
  
Use case 4: Sprawdzenie stanu konta
-----------------

  - Wybranie opcji sprawdzenia stanu konta.
  - Zadeklarowanie chęci wydrukowania potwierdzenia.
  - Zabranie karty.
  - Zabranie potwierdzenia.
  
Use case 5: Zakup kodu do telefonii pre-paidowej 
-----------------

  - Wybranie opcji zakupu kodu do telefonii pre-paidowej.
  - Wybór operatora.
  - Wprowadzenie kwoty doładowania.
  - Zabranie karty.
  - Zabranie potwierdzenia z kodem.
  
Use case 6: Wykonanie przelewu
-----------------

  - Wybranie opcji wykonanie przelewu.
  - Wprowadzenie numeru konta odbiorcy.
  - Wprowadzenie kwoty przelewu.
  - Wprowadzenie tytułu przelewu.
  - Potwierdzenie poprawności wprowadzonych danych.
  - Zabranie karty.
  - Zabranie potwierdzenia.
  
Use case 7: Uzupełnienie gotówki w bankomacie i podmiana kasetki z wpłaconymi banknotami na pustą
-----------------

  - Otworzenie bankomatu.
  - Zamknięcie bankomatu.
  - Wprowadzenie karty.
  - Wprowadzenie PIN'u.
  - Wprowadzenie ilości włożonych banknotów o zadanych nominałach.
  - Zadeklarowanie opróżnienia kasetki na wpłaty.
  - Potwierdzenie poprawności wprowadzonych danych.
  - Zabranie karty.
  
